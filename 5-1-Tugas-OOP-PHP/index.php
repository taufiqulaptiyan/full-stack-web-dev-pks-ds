<?php

class Hewan {
    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama=$nama;
        $this->darah=$darah;
        $this->jumlahKaki=$jumlahKaki;
        $this->keahlian=$keahlian;
        $this->attackPower=$attackPower;
        $this->defencePower=$defencePower;
    }

    public function atraksi()
    {
        return $this->nama." sedang ".$this->keahlian;
    }
}

class Elang extends Hewan {
    public function getInfoHewan()
    {
        echo "Nama Hewan = ".$this->nama 
            ."<br>". 
            "Jenis Hewan = Elang" 
            ."<br>". 
            "Darah = ".$this->darah 
            ."<br>".  
            "Jumlah Kaki = ".$this->jumlahKaki 
            ."<br>". 
            "Keahlian = ".$this->keahlian
            ."<br>".
            "Attack Power = ".$this->attackPower
            ."<br>".
            "Defence Power = ".$this->defencePower
            ."<br>";
    }
}
    
class Harimau extends Hewan {
    public function getInfoHewan()
    {
        echo "Nama Hewan = ".$this->nama 
            ."<br>". 
            "Jenis Hewan = Harimau" 
            ."<br>". 
            "Darah = ".$this->darah 
            ."<br>".  
            "Jumlah Kaki = ".$this->jumlahKaki 
            ."<br>". 
            "Keahlian = ".$this->keahlian
            ."<br>".
            "Attack Power = ".$this->attackPower
            ."<br>".
            "Defence Power = ".$this->defencePower
            ."<br>";
    }
}

class Fight {
    public $attackPower;
    public $defencePower;

    public function serang($nama1, $nama2)
    {
        echo "<br>";
        return $nama1." sedang menyerang ".$nama2;
    }

    public function diserang($diserang, $darah, $attPower, $defPower)
    {
        echo "<br>";
        echo $diserang." sedang diserang";
        echo "<br>";
        $tarung=$darah-$attPower/$defPower;
        echo "<br>";
        return "Darah harimau_2 menjadi ".$tarung;
    }
}

#Output
$elang = new Elang("Elang_1", 50, 2, "terbang tinggi", 10, 5);
$harimau = new Harimau('Harimau_2', 50, 4, "lari cepat", 7, 8);
$fight = new Fight();

echo "<h1>Informasi Hewan</h1>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();
echo "<br>";
    
echo "<h1>Atraksi Hewan</h1>";
echo $elang->atraksi();
echo "<br>";

echo $fight->serang($elang->nama, $harimau->nama);
echo "<br>";
echo $fight->diserang($harimau->nama, $harimau->darah, $elang->attackPower, $harimau->defencePower);
?>