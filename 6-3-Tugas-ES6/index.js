//TUGAS 17 ES6

//Soal 1
const luasPersegiPanjang = (panjang, lebar) => {
  return panjang * lebar;
};
console.log(luasPersegiPanjang(10, 5))

const kelilingPersegiPanjang = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};
console.log(kelilingPersegiPanjang(10, 5))

//Soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    }
  }
}
//Driver Code 
newFunction("William", "Imoh").fullName()

//Soal 3
let newObject = [
  'Muhammad',
  'Iqbal Mubarok',
  'Jalan Ranamanyar',
  'playing football',
]

const [firstName, lastName, address, hobby] = newObject
console.log(firstName, lastName, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

//Soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`
console.log(before)