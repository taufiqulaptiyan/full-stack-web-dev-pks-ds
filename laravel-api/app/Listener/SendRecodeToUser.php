<?php

namespace App\Listener;

use App\Events\GenerateSuccess;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\GenerateSuccessMail;

class SendRecodeToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateSuccess  $event
     * @return void
     */
    public function handle(GenerateSuccess $event)
    {
        Mail::to($event->otp->user->email)->send(new GenerateSuccessMail($event->otp));
    }
}
