<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Comments extends Model
{
    protected $fillable=['content','post_id'];
    protected $keyType='string';
    public $incrementing=false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id=Str::uuid();
        });
    }
}
