<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Posts extends Model
{
    protected $fillable=['title','description'];
    protected $keyType='string';
    public $incrementing=false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id=Str::uuid();
        });
    }
}
