<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Users;

class OtpCode extends Model
{
    protected $guarded=[];
    protected $keyType='string';
    public $incrementing=false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id=Str::uuid();
        });
    }

    public function user(){
        return $this->belongsTo('App\Users');
    }

}
