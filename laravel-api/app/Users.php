<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use App\Roles;

class Users extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $fillable=['username','email','name','password','email_verified_at','role_id'];
    protected $keyType='string';
    public $incrementing=false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->id=Str::uuid();
        });
    }

    public function role(){
        return $this->belongsTo('App\Roles');
    }

    public function otpCode(){
        return $this->hasOne('App\otpCode','user_id');
    }
}
