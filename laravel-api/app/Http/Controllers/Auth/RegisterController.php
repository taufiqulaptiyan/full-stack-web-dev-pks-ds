<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Users;
use App\OtpCode;
use App\Events\RegisterSuccess;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required',
            'username' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user=Users::create($request->all());

        do{
            $otp=mt_rand(100000, 999999);
            $check=OtpCode::where('otp',$otp)->first();

        }while($check);

        $validUntil=Carbon::now()->addMinutes(15);
        $otp_code=OtpCode::create([
            'otp'=>$otp,
            'valid_until'=>$validUntil,
            'user_id'=>$user->id
        ]);

        event(new RegisterSuccess($otp_code));

        return response()->json([
            'success'=>true,
            'message'=>'User telah dibuat, silahkan verifikasi email',
            'data'=>[
                'user'=>$user,
                'otp_code'=>$otp_code
            ]

        ]);
    }
}
